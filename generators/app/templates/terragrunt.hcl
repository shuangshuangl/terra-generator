include "root" {
  path = find_in_parent_folders()
}

include "base" {
  path   = "${dirname(find_in_parent_folders())}/_base/cmp/cmp-infra/<%= servicesName%>/<%= hclFileName%>"
  expose = true
}