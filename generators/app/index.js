"use strict";
const Generator = require("yeoman-generator");
const chalk = require("chalk");
const yosay = require("yosay");

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(
        `Welcome to the beautiful ${chalk.red(
          "generator-terragrunt-generator"
        )} generator!
        This is a simple start to generate First Time Use Terraform config for cmp-infra now.
        1. Base on input folder name, env and regions it will create terragrunt.hcl for each env.
        2. TBD. Nested folder for any AWS services as requirement.
        `
      )
    );

    const prompts = [
      {
        type: "input",
        name: "servicesName",
        message:
          "Please input your services folder name e.g. dt-service-cmp-device-connector",
        default: "dt-service-cmp-device-connector",
        validate: input => input.length > 0,
        store: true
      },
      {
        type: "input",
        name: "hclFileName",
        message:
          "Please input base .hcl file name locate in the included base path, e.g. service-role.hcl",
        default: "service-role.hcl",
        validate: input => input.length > 0,
        store: true
      },
      {
        type: "input",
        name: "environments",
        message:
          "What logical environments will you be running, we normally do env one by one (separate multiple responses by comma)?",
        default: "development,qa",
        validate: input => input.length > 0,
        store: true
      },
      {
        type: "input",
        name: "regions",
        message:
          "Which AWS regions you need to put terragrunt.hcl in (separate multiple responses by comma)?",
        default: "us-east-1",
        validate: input => input.length > 0,
        store: true
      }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {
    const hclFileName = this.props.hclFileName;
    const servicesName = this.props.servicesName;
    const envs = this.props.environments.split(",");
    const regions = this.props.regions.split(",");

    for (let env of envs) {
      for (let region of regions) {
        // Do for each env
        this.fs.copyTpl(
          // Copy from
          this.templatePath("terragrunt.hcl"),
          // Paste to
          this.destinationPath(
            `target/${env}/${region}/cmp/cmp-infra/${servicesName}/terragrunt.hcl`
          ),
          {
            servicesName: servicesName,
            hclFileName: hclFileName
          }
        );
      }
    }
  }

  install() {}
};
