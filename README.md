# generator-terragrunt-generator [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Simple terragrunt hcl generator

## Installation

First, install [Yeoman](http://yeoman.io) and generator-terragrunt-generator using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-terragrunt-generator
```

Then generate your target config for terragrunt under https://gitlab.com/rivian/dc/dc-terraform :

```bash
yo generator-terragrunt-generator
```

### Note: This is a simple start to generate First Time Use Terraform config for cmp-infra now.
    1. Base on input folder name, env and regions it will create terragrunt.hcl for each env.
    2. TBD. Nested folder for any AWS services as requirement.

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License
Owned by Rivian


[npm-image]: https://badge.fury.io/js/generator-terragrunt-generator.svg
[npm-url]: https://npmjs.org/package/generator-terragrunt-generator
[travis-image]: https://travis-ci.com/Rivian/generator-terragrunt-generator.svg?branch=master
[travis-url]: https://travis-ci.com/Rivian/generator-terragrunt-generator
[daviddm-image]: https://david-dm.org/Rivian/generator-terragrunt-generator.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/Rivian/generator-terragrunt-generator
